import utils from 'util';
import * as test_utils from 'test-utils';

const params = { password: 'sdf234fdsf32cdsc' };

const myPromice = utils.promisify(test_utils.runMePlease);

try {
  const result = await myPromice(params);
  console.log(result);
} catch (error) {
  console.log(error);
}